# Fractal 

Mandelbrot Set

### Complie
gcc -std=c++11 Main.cpp

### CMake
cmake_minimum_required(VERSION 3.10)

project("Mandelbrot-Fractal")

set(CMAKE_CXX_STANDARD 11)

add_executable(Main.cpp)
